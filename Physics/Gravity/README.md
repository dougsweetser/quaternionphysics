# The Current State of a Quaternion Gravity Proposal

by Doug Sweetser

## The Back Story

In the Spring of 2015, the first step toward a novel quaternion gravity
proposal was made. Once again I was staring at a quaternion squared:

[''' (t, x, y, z)^2 = (t - x^2 - y^2 - z^2, 2 t x, 2 t y, 2 t z ) ''']

I have a fuzzy memmory of asking the news group sci.physics.research what I
should call the three terms that are not the Lorentz invariant interval and not
getting an answer. I found a name for them: space-times-time. A beast must be
named before being tamed. I recalled a quote for the introduction of Abraham
Pais' scientific biograph, "Subtle is the Lord...". he wrote:

> Were I to write a one-sentence biography of Einstein, I would say, 'He was
> the freest man I have ever known.' Had I to compose a one-sentence sceintific
> biography of him, I would write, 'Better than anyone before or after him, he
> knew how to invent invariance principles and make use of statistical
> fluctuations.'

By contrast, I feel defined by constraints, from the daily medical issues
imposed by diabetes to the mundane job with computers that has nothing to do
with physics. Since I embrace the long view, I am happy with the research
progress made by a person in my situation.

It was the scientific biography I was thinking about. Special relativity was
the result of an invariance principle Einstein "invented". Once I had named
space-times-time, my question was what if space-times-time was an invariance
principle? What sort of physics would that describe? If the space-times-time
was invariant, then the interval would have to vary. That could be gravity. If
Einstein had been using quaternions, he would have explored space-times-time
invairance as an invariance principle.

## Relaxed Relativity as a Transformation Law

A symmetry must be paired with a transformation law. The transformation law
shows how one moves around in a way consistent with the symmetry. I did nothing
to advance this needed step. Sometine in early 2018 in a YouTube comment,
Purple Penguin flushed out the idea. One measures time and space in the ways
detailed in special relativity. What is "relaxed" is that one does not insist
that all observers are inerial. As a consequence, observers will not agree
upon the measurements of the speed of light.

Different transformations are allowed. With the right choices of functions, one
can write transformation equations that only deal with either changes in space
or changes in time:

['''dt" = 1/\gamma dt ''']

['''dR" = \gamma dR ''']

Try to take the smallest possible step away from special relativity into
relaxed relativity. There are two observers, one at the botton of a building,
the other at the top. The observer at the top of the building sets up an
experiment to measure the speed of light, getting values for a change in space,
dR, and a change in time, dt. The top observer confirms the ratio of dR/dt is
the speed of light, precisely as expected.

The observer at the bottom makes observations of the observer at the top
measuring the speed of light. The observer at the bottom is able to get both
the measurements of differences in space, dR', and differences in time, dt'. The
ratio of these two is _not_ equal to the speed of light. Instead, the product
of dR' dt' = dR dt. 

## A Flat Base Space and Curved Tangent Space

A commonly repeated statement is that space-time is curved by gravity. When one
looks at the Schwarzschild solution of the general relativity, notice that the
metric under analysis uses dt and dR for a differential like so (MTW, p. xxx):

[''' d \tau^2 = (1 - 2 G M/c^2 R) dt^2 - \frac{1}{1 - 2 G M/c^2 R} dR^2 ''']

This equation is a discription of phase space, not the base space of space-time
which would be written using $t^2$ and $R^2$. Physicist understand this point.
Move to the next place in a gravity field, and adjustment may be required. One
has to integrate over all infinitely small steps taken.

While using phase space may be common in the differential geometry world of
general relativity, there is another way to describe the results that is also
understood by physicists. Divide through by the [' \d \tau^2 '], multiply by an
[' m^2 '], and the Schwarzschild solution is about energy and momentum:

[''' m^2 = (1 - 2 G M/c^2 R) E^2 - \frac{1}{1 - 2 G M/c^2 R} E^2 ''']

This is the same information rearranged. Yet energy and momentum are natural
players in other areas of physics. What a gravitational field does is change
how an observer sees the energy and momentum of a system in a different
location in the gravitational field.

## The Missing Velocity Field Equations

This area of study is missing something essential. There is no justification
for the gescape velocity field. _IF_ for a point gravitation source velocity
field is:

[''' \gamma = \exp(G M / c^2 R) ''']

As the mass approaches zero, this approaches unity. Look at the square when one
uses this velocity field in combination with the hypothesis that gravity
conserves energy-times-momentum

[''' (dt, dR) \rightarrow (dt", dR") = (dt/\gamma, \gamma dR) ''']

[''' (dt", dR")^2 = (\exp(-2 G M/c^2 R) dt^2 - \exp(+2 G M/c^2 R) dR, 2 dt dR) ''']


