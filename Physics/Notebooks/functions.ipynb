{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions for Quaternions and Quaternion Series"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A function is a map from one set to another. For my work with quaternions and quaternion series, there are four permutations: \n",
    "\n",
    "1. A function $f$ from one quaternion $q_1$ to one quaternion $q_2$: $f(q_1) = q_2$\n",
    "\n",
    "1. A function $fs$ from one quaternion series $qs_1$ to one quaternion series $qs_2$: $fs(qs_1) = qs_2$\n",
    "\n",
    "1. A function $g$ from one quaternion $q_1$ to one quaternion series $qs_2$: $g(q_1) = qs_2$\n",
    "\n",
    "1. A function $p$ from one quaternion series $qs_1$ to one quaternion $q_2$: $p(qs_1) = q_2$\n",
    "\n",
    "I tried to be a little precise in my language, using the word \"one\" above instead of \"a\". Since a quaternion series is a collection of quaternions, it is too easy to think of them as many quaternions instead of exactly one set of quaternions. Examples of each will be provided."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It was interesting to see how one could translate from a python function to one a mathematician would recognize. The library used for quaternion functions was refactored from an object-oriented approach to a functional one. This made the role of mapping from one set to another more clear. Unlike programming in languages like C or C++, Python discourages something known as polymorphisms: the same function doing similar things. Our math language is in favor of polymorphisms. Consider the popular word \"add\". Two numbers can be added or two metric spaces can be added together. We understand these are not the same operations, but are allowed to write exactly the same word."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Qs library only has one function named \"sin\". It takes a quaternion and using the sin() returns one quaternion. Here is a simple picture to represent the mapping:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](images/functions_f_q_q.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The domain is all possible quaternions and the co-domain is all possible quaternions. By starting with one quaternion, the range is one quaternion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The call to the sine function looks like this:\n",
    "```\n",
    "def sin(q_1: Q) -> Q:\n",
    "\t…\n",
    "\tReturn q_result\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Everyone is a quaternion, the class Q. This works no matter what the representation happens to be."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What about a quaternion vector space? First, what is a quaternion vector space? That is implemented using the class Qs which is composed of dimension _d_ quaternions of the class Q. I will also call this a quaternion series. Because the quaternion class Qs is a vector space, they can be added together and multiplied by a scalar. One can creation a function for the vector space, say the sine function again."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](images/functions_fs_qs_qs.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The software cannot officially deal with infinite dimensional quaternion series, but tens of thousands have been done. Another imprecision of the visual is the points make each series look like separate quaternions when in fact they need to be linked to make one set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function call will look the same except for the additional factors of \"s\":\n",
    "```\n",
    "def sins(q_1: Qs, q_2: Qs) -> Qs:\n",
    "\t…\n",
    "\tReturn q_result\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how the name of the function has changed to help spot this function applies to a quaternion series, not a quaternion. This pattern follows for all the functions; sin() vs sins(), exp() vs exps(), and add() vs adds(). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For animations, many quaternions are needed. It is convenient to start from either a quaternion or a quaternion function and generate a large quaternion series. The domain is one quaternion or quaternion function, and the range is one quaternion series."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](images/functions_g_q_qs.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall that if an algebraic operation starts with one value in the domain, and ends with two values in the range, that operation is not a function (and I don't know what to call it). The visualization suggests we are going from one quaternion for $q_1$ to four for $qs_2$. The range however needs to be viewed as exactly one quaternion series with an internal dimension of 4. The generator function g is a function in the typical sense of the word. That is reflected in its definition in the code:\n",
    "\n",
    "```python\n",
    "def generate_Qs(func: FunctionType, q_1: Union[Q, FunctionType], dim: int = 10, qs_type: str = \"ket\") -> Qs:\n",
    "```\n",
    "\n",
    "This was the first function in the library that takes functions as input. The output is one quaternion series Qs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are there any functions that do the reverse, to go from a quaternion series to a quaternion? In earlier versions, the answer was technically no. One can be technical simply by looking at all the signatures of the functions.\n",
    "\n",
    "There are functions that can generate scalar quaternion series, those with a dimension of 1 so exactly equivalent to a quaternion. Those functions include abs_of_qs(), norm_squareds(), trace(), and sometimes products(), the \"s\" to imply that it takes quaternion series as the domain. The products() function will only generate a scalar if the dimension of the bra quaternion is the same as the ket quaternion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](images/functions_d_qs_q.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A function dot_product() has been defined that takes in 2 quaternion series and outputs one quaternion Q. The dot_product in general will not be real-valued. The craft of quantum mechanics is finding situations where two quaternion states may be acted upon yet the dot_product() remains real."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
