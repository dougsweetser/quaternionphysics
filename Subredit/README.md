# Subredit

The idea is to market the Quaternion Physics Research, hoping to find a few
skilled folks. Reddit might work, and it might not. But it could be work. Might
as well give it a try. I like the casual 

## name

_no spaces, e.g., "books" or "bookclub". avoid using solely trademarked names, 
e.g. use "FansOfAcme" instead of "Acme". once chosen, this name cannot be changed._

This is THE most important decision to be made, the one phrase that has the
goal of this group. I had lots of useful discussions in a moderated newsgroup
in the late 90s, early 2000s, called sci.physics.research. Here is my variation
on that name:

**QuaternionPhysicsResearch**

The group will be focused only on putting quaternions to work. I consider this
research, trying to do new things.

## title

_e.g., books: made from trees or pixels. recommendations, news, or thoughts_

Reconstructing physics one equation at a time using quaternions and quaternion
series.

## description

_Appears in search results and social media links. 500 characters max._

A civil discussion of rebuilding any and all work done with tensors in physics
with quaternions (a division algebra) and quaternion series (a semi-group with
inverses). Possible rewards include a novel approach to gravity, a new way to
understand quantum mechanics as the systematic understanding of patterns of
space-like events by an observer at the observer's origin, and analytic
animations to see what physics equations mean.

(422 characters)

## Sidebar

_shown in the sidebar of your subreddit. 10240 characters max._


## submission text

_text to show on submission page. 1024 characters max._

## type

    Pick one:
    public	anyone can view and submit
    restricted	anyone can view, but only some are approved to submit links
    private	only approved members can view and submit
    premium only[?]	only reddit premium members can view and submit

**public**  The idea is to find people who have math and physics chops who are
frusted enough with standard approaches to mathematical physics to bring their
vision to the project.

## content options

    any	any link type is allowed
    links only	only links to external sites are allowed
    text posts only	only text/self posts are allowed
    Custom label for submit link button (blank for default):
    Submit a new link
    Custom label for submit text post button (blank for default):
    Submit a new text post

Try "any", see if more constraints are needed.

## wiki

    disabled	Wiki is disabled for all users except mods
    mod editing	Only mods, approved wiki contributors, or those on a page's edit list may edit
    anyone	Anyone who can submit to the subreddit may edit
    Subreddit karma required to edit and create wiki pages:
    100
    Account age (days) required to edit and create wiki pages:
    0

It is unclear how passive/aggressive one should be about wikis... Let me read
up about it on reddit. Mod editing sounds about right.


## other options

    viewers must be over eighteen years old
    allow this subreddit to be exposed to users in /r/all, /r/popular, default, and trending lists
    allow this subreddit to be exposed to users who have shown intent or interest through discovery and onboarding
    enable marking posts as containing spoilers
    show thumbnail images of content
    expand media previews on comments pages
    allow image uploads and links to image hosting sites
    allow video uploads
    allow free-form reports by users
    exclude posts by site-wide banned users from modqueue/unmoderated
    collapse deleted and removed comments
    beta - enable marking posts as Original Content (OC) on the desktop redesign
    beta - mark all posts in this subreddit as Original Content (OC) on the desktop redesign
    suggested comment sort (all comment threads will use this sorting method by default)
    Minutes to hide comment scores:
    0

